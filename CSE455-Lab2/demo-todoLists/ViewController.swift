//
//  ViewController.swift
//  demo-todoLists
//
//  Created by Tan Do on 1/20/17.
//  Copyright © 2017 Tan Do. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myText: UITextField!
    
    @IBOutlet weak var txtOutput: UITextView!
    //empty array string to store items in this project
    var items: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func myButton(_ sender: Any) {
        //check to see myText (textfield) is empty
        if(myText.text! == "") {
            //return nothing because we dont want user to enter nothing in the list when tap Add button
            return
        }
        //Using append to Insert data when user enter into the textfield each time user tap Add button
        items.append(myText.text!)
        //Makesure the to do list is empty at first
        txtOutput.text = ""
        //scan thru each item in the items array
        for item in items {
            //if we have the todo list (output) empty, we are going to put each of the items (item) inside the todo lists. we add a new line after each item.
            txtOutput.text.append("\(item)\n")
        }
        //clear the textfield. Make sure the user dont add same items everytime they hit the Add
        myText.text = ""
        //dismiss the keyboard after Add button is tapped
        myText.resignFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

